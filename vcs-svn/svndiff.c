/*
 * Licensed under a two-clause BSD-style license.
 * See LICENSE for details.
 */

#include "cache.h"
#if 0
#include "git-compat-util.h"
#endif
#include "line_buffer.h"

/*
 * svndiff0 applier
 *
 * See http://svn.apache.org/repos/asf/subversion/trunks/notes/svndiff.
 *
 * svndiff0 ::= 'SVN\0' window window*;
 * window ::= int int int int int instructions inline_data;
 * instructions ::= instruction*;
 * instruction ::= view_selector int int
 *   | copyfrom_data int
 *   | packed_view_selector int
 *   | packed_copyfrom_data
 *   ;
 * view_selector ::= copyfrom_source
 *   | copyfrom_target
 *   ;
 * copyfrom_source ::= # binary 00 000000;
 * copyfrom_target ::= # binary 01 000000;
 * copyfrom_data ::= # binary 10 000000;
 * packed_view_selector ::= # view_selector OR-ed with 6 bit value;
 * packed_copyfrom_data ::= # copyfrom_data OR-ed with 6 bit value;
 * int ::= highdigit* lowdigit;
 * highdigit ::= # binary 10000000 OR-ed with 7 bit value;
 * lowdigit ::= # 7 bit value;
 */

#define SVNDIFF_MAGIC "SVN\0"

#define INSN_MASK	0xc0
#define INSN_COPYFROM_SOURCE	0000
#define INSN_COPYFROM_TARGET	0x40
#define INSN_COPYFROM_DATA	0x80
#define OPERAND_MASK	0x3f

#define VLI_CONTINUE	0x80
#define VLI_DIGIT_MASK	0x7f
#define VLI_BITS_PER_DIGIT 7

struct view {
	FILE *file;
	char *buf;
	size_t off;
	size_t len;
};

struct window {
	char *out;
	size_t out_off;
	size_t out_len;

	const char *preimage;
	size_t preimage_off;
	size_t preimage_len;
	const char *instructions;
	size_t instructions_len;
	const char *data;
	size_t data_len;
};

static size_t fskip(FILE *file, size_t nbytes)
{
	static char buf[4096];
	size_t done = 0;

	/*
	 * Try to throw away nbytes bytes.
	 */
	while (done < nbytes) {
		size_t n = nbytes - done;
		size_t in;

		if (n > sizeof(buf))
			n = sizeof(buf);
		in = fread(buf, 1, n, file);
		done += in;
		if (in != n)
			break;
	}
	return done;
}

static int move_window(struct view *view, size_t newoff, size_t newlen)
{
	size_t oldoff, oldlen, len;
	char *oldbuf, *buf;

	if (newoff > SIZE_MAX - newlen)
		return error("Source file is too big: "
			"%"PRIu64" + %"PRIu64" > SIZE_MAX",
			(uint64_t) newoff, (uint64_t) newlen);
	assert(view && view->file);

	oldoff = view->off;
	oldlen = view->len;
	oldbuf = view->buf;

	assert(oldoff <= SIZE_MAX - oldlen);
	assert(!oldlen || oldbuf);
	if (newoff < oldoff || newoff + newlen < oldoff + oldlen)
		return error("Corrupt delta? Window slides left.");

	view->off = newoff;
	view->len = newlen;
	view->buf = buf = malloc(newlen);
	if (newlen && !buf)
		return error("Address space exhausted; "
				"no room for source window");
	if (newoff < oldoff + oldlen) {
		const size_t overlap = oldoff + oldlen - newoff;
		buf = mempcpy(buf, oldbuf + oldlen - overlap, overlap);
		newlen -= overlap;
	}
	if (oldoff + oldlen < newoff) {
		const size_t gap = newoff - oldoff - oldlen;
		if (fskip(view->file, gap) != gap)
			return error("error seeking in source: %s",
							strerror(errno));
	}
	len = fread(buf, 1, newlen, view->file);
	if (len == newlen)
		return 0;
	if (feof(view->file)) {
		warning("Overlarge window specified; adjusting.");
		view->len -= (newlen - len);
		return 0;
	}
	return error("error reading source: %s", strerror(errno));
}

static int buf_parse_int(size_t *result, const char **buf, size_t *len)
{
	const char *p = *buf;
	size_t bufsz = *len;
	size_t rv = 0;
	while (bufsz) {
		int ch = (unsigned char) *p++;
		bufsz--;
		rv <<= VLI_BITS_PER_DIGIT;
		rv += ch & VLI_DIGIT_MASK;
		if (!(ch & VLI_CONTINUE)) {
			*result = rv;
			*buf = p;
			*len = bufsz;
			return 0;
		}
	}
	return error("Corrupt delta? Argument with leading digits "
			"%"PRIu64" left unterminated.", (uint64_t) rv);
}

static int parse_int(size_t *result, size_t *len)
{
	size_t bufsz = *len;
	size_t rv = 0;
	while (bufsz) {
		int ch = buffer_read_char();
		if (ch == EOF)
			break;
		bufsz--;
		rv <<= VLI_BITS_PER_DIGIT;
		rv += (ch & VLI_DIGIT_MASK);
		if (!(ch & VLI_CONTINUE)) {
			*result = rv;
			*len = bufsz;
			return 0;
		}
	}
	return error("Corrupt delta? Integer with leading digits "
			"%"PRIu64" left unterminated.", (uint64_t) rv);
}

static int copyfrom_source(char **out_pos, struct window *ctx, size_t nbytes)
{
	char *p = *out_pos;
	size_t offset;
	if (buf_parse_int(&offset, &ctx->instructions,
					&ctx->instructions_len))
		return -1;
	if (offset > SIZE_MAX - nbytes)
		return error("Source data is too long: "
				"%"PRIu64" + %"PRIu64" > SIZE_MAX",
				(uint64_t) offset, (uint64_t) nbytes);
	if (offset < ctx->preimage_off)
		return error("Corrupt delta? Attempts to write source "
				"data that has been forgotten already.");
	if (ctx->preimage_len < nbytes)
		return error("Corrupt delta? Attempts to write source "
				"data that has not been read yet.");
	if (p + nbytes > ctx->out + ctx->out_len)
		return error("Corrupt delta? Attempts to write "
				"source data without making room for it.");
	p = mempcpy(p, ctx->preimage - ctx->preimage_off + offset, nbytes);
	*out_pos = p;
	return 0;
}

static int copyfrom_target(char **out_pos, struct window *ctx, size_t nbytes)
{
	char *p = *out_pos;
	size_t offset;
	if (buf_parse_int(&offset, &ctx->instructions,
					&ctx->instructions_len))
		return -1;
	if (offset > SIZE_MAX - nbytes)
		return error("Output data is too long: "
				"%"PRIu64" + %"PRIu64" > SIZE_MAX",
				(uint64_t) offset, (uint64_t) nbytes);
	if (offset < ctx->out_off)
		return error("Corrupt delta? Attempts to copy target "
				"data that has been written and forgotten.");
	if (p <= ctx->out - ctx->out_off + offset)
		return error("Corrupt delta? Attempts to copy data "
				"that has not been written yet.");
	if (ctx->out_len < nbytes)
		return error("Corrupt delta? Attempts to copy target "
				"data that has not even been allocated.");
	if (p + nbytes > ctx->out + ctx->out_len)
		return error("Corrupt delta? Attempts to copy target "
				"data without making room for it.");
	while (nbytes) {
		*p++ = (ctx->out - ctx->out_off)[offset++];
		nbytes--;
	}
	*out_pos = p;
	return 0;
}

static int copyfrom_data(char **out_pos, struct window *ctx,
				size_t *data_pos, size_t nbytes)
{
	char *p = *out_pos;
	size_t offset = *data_pos;
	if (offset > SIZE_MAX - nbytes)
		return error("Inline data is too long: "
				"%"PRIu64" + %"PRIu64" > SIZE_MAX",
				(uint64_t) offset, (uint64_t) nbytes);
	if (ctx->data_len < offset + nbytes)
		return error("Corrupt delta? Attempts to read inline "
				"data where none exists.");
	if (p + nbytes > ctx->out + ctx->out_len)
		return error("Corrupt delta? Attempts to write inline "
				"data without making room for it.");
	*data_pos = offset + nbytes;
	p = mempcpy(p, ctx->data + offset, nbytes);
	*out_pos = p;
	return 0;
}

static int apply_one_instruction(struct window *ctx,
				char **out, size_t *data_pos)
{
	unsigned char instruction;
	size_t nbytes;

	assert(ctx->instructions_len);
	assert(ctx->instructions);

	instruction = (unsigned char) *ctx->instructions;
	ctx->instructions++;
	ctx->instructions_len--;
	nbytes = instruction & OPERAND_MASK;
	if (!nbytes && buf_parse_int(&nbytes, &ctx->instructions,
						&ctx->instructions_len))
		return -1;
	switch (instruction & INSN_MASK) {
	case INSN_COPYFROM_SOURCE:
		return copyfrom_source(out, ctx, nbytes);
	case INSN_COPYFROM_TARGET:
		return copyfrom_target(out, ctx, nbytes);
	case INSN_COPYFROM_DATA:
		return copyfrom_data(out, ctx, data_pos, nbytes);
	default:
		return error("Delta contains invalid instruction %x",
					(unsigned int) instruction);
	}
}

static int do_instructions(struct window *ctx)
{
	char *p = ctx->out;
	size_t data_offset = 0;

	/*
	 * Advance p while copying data from the source, target,
	 * and inline data views.
	 */
	while (ctx->instructions_len)
		if (apply_one_instruction(ctx, &p, &data_offset))
			return -1;
	return 0;
}

static int apply_within(const struct view *preimage, size_t *out_offset,
			FILE *outf, size_t *len_remaining,
			size_t *out_remaining)
{
	struct window ctx;

	ctx.preimage_len = preimage->len;
	ctx.preimage_off = preimage->off;
	ctx.preimage = preimage->buf;

	assert(out_offset);
	assert(len_remaining);

	/*
	 * Window header:
	 *  - "source view" offset and length (already handled);
	 *  - "target view" length;
	 *  - "instructions" length;
	 *  - inline data length.
	 */

	if (parse_int(&ctx.out_len, len_remaining))
		return -1;
	ctx.out_off = *out_offset;

	if (parse_int(&ctx.instructions_len, len_remaining))
		return -1;
	if (parse_int(&ctx.data_len, len_remaining))
		return -1;

	if (ctx.instructions_len > SIZE_MAX - ctx.data_len)
		return error("Instructions too long: "
					"%"PRIu64" + %"PRIu64" > SIZE_MAX",
					(uint64_t) ctx.instructions_len,
					(uint64_t) ctx.data_len);
	if (ctx.instructions_len > *len_remaining)
		warning("Delta instructions appear to be tructated "
				"(%"PRIu64" bytes out of %"PRIu64")",
				(uint64_t) *len_remaining,
				(uint64_t) ctx.instructions_len);
	if (ctx.instructions_len + ctx.data_len > *len_remaining)
		warning("Delta appears to be truncated");

	ctx.out = malloc(ctx.out_len);
	if (ctx.out_len && !ctx.out)
		return error("Address space exhausted; "
					"no room for target window");
	ctx.out_off = *out_offset;
	ctx.instructions = buffer_read_string(
					ctx.instructions_len + ctx.data_len);
	*len_remaining -= ctx.instructions_len + ctx.data_len;
	ctx.data = ctx.instructions + ctx.instructions_len;

	if (do_instructions(&ctx)) {
		free(ctx.out);
		return -1;
	}
	if (ctx.out_len > *out_remaining)
		ctx.out_len = *out_remaining;
	if (fwrite(ctx.out, 1, ctx.out_len, outf) == ctx.out_len) {
		/* Success. */
		free(ctx.out);
		*out_offset = ctx.out_off;
		*out_remaining -= ctx.out_len;
		return 0;
	}
	error("Error writing patched file: %s", strerror(errno));
	free(ctx.out);
	return -1;

}

/*
 * Magic bytes ("SVN\0")
 */
static int parse_magic(size_t *len)
{
	const char *buf;
	const size_t magic_len = 4;

	if (*len < magic_len)
		return error("Invalid diff stream: no file type header");
	buf = buffer_read_string(magic_len);
	if (memcmp(buf, SVNDIFF_MAGIC, magic_len))
		return error("Unrecognized file type %s", buf);
	*len -= magic_len;
	return 0;
}

/*
 * Apply a delta from the file named by delta, truncated to len bytes.
 */
int svndiff0_apply(const char *delta, size_t len, size_t out, size_t in,
				FILE *preimage, FILE *postimage)
{
	struct view preimage_view = {preimage, NULL, 0, 0};
	size_t out_offset = 0;

	assert(delta && preimage && postimage);
	if (buffer_init(delta))
		return error("cannot open %s: %s\n", delta, strerror(errno));
	if (parse_magic(&len))
		return -1;
	while (len > 0 && out > 0) {
		/* New window. */
		size_t pre_off, pre_len;
		int ch;
		if (parse_int(&pre_off, &len))
			return -1;
		if (parse_int(&pre_len, &len))
			return -1;
		if (pre_len > in)
			pre_len = in;
		if (move_window(&preimage_view, pre_off, pre_len))
			return -1;
		in -= pre_len;
		if (apply_within(&preimage_view, &out_offset, postimage, &len, &out))
			return -1;
		if (ferror(stdin) || feof(stdin))
			break;
	}
	return buffer_deinit();
}
