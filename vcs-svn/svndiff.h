#ifndef SVNDIFF_H_
#define SVNDIFF_H_

/*
 * Read in a delta using the line_buffer library and use it to
 * patch preimage, writing the result to postimage.
 */
extern int svndiff0_apply(const char *delta, size_t len, size_t out, size_t in,
				FILE *preimage, FILE *postimage);

#endif
