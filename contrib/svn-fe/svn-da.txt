svn-da(1)
=========

NAME
----
svn-da - Apply a given diff0 to a given source and produce fulltext

SYNOPSIS
--------
svn-da DIFF [[[SOURCE_LEN] DEST_LEN] DELTA_LEN] <SOURCE

DESCRIPTION
-----------
Applies an svndiff version 0 to a given source file.

TODO
----
* Allow the first n (typically 0 or 5) bytes of output to be skipped

SEE ALSO
--------
https://svn.apache.org/repos/asf/subversion/trunk/notes/svndiff
