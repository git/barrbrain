/*
 * This file is in the public domain.
 * You may freely use, modify, distribute, and relicense it.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "svndiff.h"

int main(int argc, char **argv)
{
	size_t out = SIZE_MAX;
	size_t in = SIZE_MAX;
	size_t len = SIZE_MAX;
	if (argc < 2 || argc > 5) {
		fprintf(stderr, "usage: svn-da delta "
			"[[[postimage_length] preimage_length] delta_length] "
			"<preimage >postimage");
		return 1;
	}
	if (argc >= 3)
		out = strtoull(argv[2], NULL, 10);
	if (argc >= 4)
		in = strtoull(argv[3], NULL, 10);
	if (argc >= 5)
		len = strtoull(argv[4], NULL, 10);
	if (svndiff0_apply(argv[1], len, out, in, stdin, stdout))
		return 1;
	return 0;
}
