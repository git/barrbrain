/*
 * test-svn-fe: Code to exercise the svn import lib
 */

#include "git-compat-util.h"
#include "vcs-svn/svndump.h"
#include "vcs-svn/svndiff.h"

int main(int argc, char *argv[])
{
	if (argc == 3 && !strcmp(argv[1], "-d"))
		return svndiff0_apply(argv[2], SIZE_MAX, stdin, stdout);

	if (argc != 2)
		usage("test-svn-fe [-d] <file>");

	svndump_init(argv[1]);
	svndump_read(NULL);
	svndump_deinit();
	svndump_reset();
	return 0;
}
